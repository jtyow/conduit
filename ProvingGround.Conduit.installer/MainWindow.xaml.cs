﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Security;
using System.Security.AccessControl;

using Microsoft.Win32;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProvingGround.Conduit.installer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Target Paths
        private string _targetRevit2015AddinPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Autodesk", "Revit", "Addins", "2015"); //Revit Addins Folder
        private string _targetRevit2016AddinPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Autodesk", "Revit", "Addins", "2016"); //Revit Addins Folder
        private string _targetGrasshopperPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Grasshopper", "Libraries"); //Grasshopper Addons Folder
        private string _targetDynamoPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Dynamo", "0.8", "packages"); //Dynamo Addons Folder

        private string _targetProvingGroundRootFolder = @"C:\Proving Ground";
        private string _grasshopperDatasetPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Proving Ground", "Conduit");

        // Source path
        private string _sourcepath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        /// <summary>
        /// Constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            form_EULA m_eula = new form_EULA();
            m_eula.ShowDialog();

            if (m_eula.Acceptance == false)
            {
                this.Close();
            }

            Label_Version.Content = "v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

        }

        /// <summary>
        /// Install new files
        /// </summary>
        private void InstallFiles()
        {
            try
            {
                DirectoryInfo m_dirSource = new DirectoryInfo(_sourcepath);
                FileInfo[] m_files = m_dirSource.GetFiles("*.*");

                // iterate through files
                foreach (FileInfo x in m_files)
                {
                    if (x.Name == "ProvingGround.Conduit.dll")
                    {
                        x.CopyTo(System.IO.Path.Combine(_targetGrasshopperPath, "ProvingGround.Conduit.gha"));
                    }

                }
            }
            catch { }
        }

        /// <summary>
        /// Install Grasshopper Dataset
        /// </summary>
        private void InstallGrasshopperDataset()
        {
            try
            {
                DirectoryInfo m_dirSource = new DirectoryInfo(System.IO.Path.Combine(_sourcepath, "Dataset"));

                FileInfo[] m_files = m_dirSource.GetFiles("*.*");

                if (System.IO.Directory.Exists(_grasshopperDatasetPath) == false)
                {
                    System.IO.Directory.CreateDirectory(_grasshopperDatasetPath);
                }

                // iterate through files
                foreach (FileInfo x in m_files)
                {
                    // Dependencies
                    if (x.Name.Contains("gh"))
                    {
                        x.CopyTo(System.IO.Path.Combine(_grasshopperDatasetPath, x.Name), true);
                    }
                }

            }
            catch { }
        }

        /// <summary>
        /// Remove old, out of date files from previous versions
        /// </summary>
        private void CleanUpOldFiles()
        {
            //add fill paths here
            List<string> m_oldfiles = new List<string>();

            // delete files
            foreach (string f in m_oldfiles)
            {
                if (System.IO.File.Exists(f))
                {
                    System.IO.File.Delete(f);
                }
            }
        }

        /// <summary>
        /// Close Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Install Tools
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Install_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // install the files
                if (System.IO.Directory.Exists(_targetGrasshopperPath))
                {
                    InstallFiles();
                    InstallGrasshopperDataset();
                    System.Windows.Forms.MessageBox.Show("Conduit was installed successfully to " + _targetGrasshopperPath, "Install Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("There was a problem installing Conduit. Are you sure Grasshopper is installed?", "Install Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
                this.Close();
            }
            catch { }
        }

        /// <summary>
        /// Drag Window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
